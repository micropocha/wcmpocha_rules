(async () => {
  class wcmPochaRules extends HTMLElement {
    constructor() {  
      const self=super();
      // Init default variables 
      self._amount = 0;
      self._selectedRuleset = null;
      self._selectedRuleFlag = null;
      // create shadowdoom
      self._root = self.attachShadow({mode: 'closed'});
      self.template = self._templateFactory()
      self._root.appendChild ( document.importNode(self.template.content, true) );
      //schedule rendering Rules to when elements were ready
      self.scheduleRenderRules();
      return self;
      }

    _templateFactory(){
      const css = `
      :host{
        display: block;
      }
      .myclass {
        color: blue;
      }
      .ruleflags {
        color: black;
      }
      form {
        display: inline;
      }
      `;
      const html = `
      <style>${css}</style>
      <template id="rule">
      <button class="rulebutton"></button> options: [ <form class="ruleflags"></form> ] 
      </template>

      <template id="ruleflag">
        <label for="redButton">Red</label>
        <input name="ruleflag" type="radio" id="redButton">
      </template>
      
      <span class="myclass">
      <em>rulesets:</em>
      <span id="rulegap"><span>
      </span>
      `;
      var wrapper = document.createElement("template")
      wrapper.innerHTML = html;
      return wrapper;
    }
    
    scheduleRenderRules() {
      var me = this;
      document.addEventListener("DOMContentLoaded", function (event) {
        console.log("DOM fully loaded and parsed");
        me.renderRules();
      });
    }

    renderRules(){
      var hook = this._root.getElementById("rulegap");
      var template = this._root.getElementById("rule");
      var ruleChilds = this.children;
      var ruleChildNodes = this.childNodes;
      for (var index = 0 ; index < ruleChilds.length; index ++ ){
        var rule = ruleChilds[index];
        var clone = document.importNode(template.content, true);
        var button = clone.querySelector('button')
        button.textContent = rule.name;
        button.rule = rule;
        button.onclick = (e) => {
          this._setActiveRule(e)
        }
        this.renderRoundFlags(clone, rule.roundFlags )
        hook.appendChild(clone);
      }
    }

    renderRoundFlags(parentElement, roundFlags){
      if (roundFlags){
        var targetElement = parentElement.querySelector('.ruleflags');
        var template = this._root.getElementById("ruleflag");
        var clickHandle =  e => {
          this._setActiveRoundFlags (e)
        }
        var addRoundFlag = ( text, selected ) => {
          var clone = document.importNode(template.content, true);
          var label = clone.querySelector('label');
          label.innerHTML=text;
          label.setAttribute("for",text);
          var input = clone.querySelector('input');
          input.setAttribute('id',text);
          input.checked = selected;
          input.onclick = clickHandle;
          targetElement.appendChild(clone);
        }
        //addRoundFlag('none', true)
        roundFlags.forEach((flag) => {
          addRoundFlag(flag, false)
        })
      }
    }

    _setActiveRule (e){
      var rule = e.target.rule;
      this._fireRulesetChanged(rule);
      this.setAttribute("selectedRuleset",rule.name);
      this._selectedRuleset = rule;
    }

    get selectedRuleset() {
      return this._selectedRuleset;
    }

    _fireRulesetChanged (rule) {
      this.dispatchEvent(new CustomEvent('rulesetChanged', {detail: {name: rule.name, rule: rule}, bubbles: true}))
    }

    _setActiveRoundFlags (e) {
      var button = e.target;
      var flag = button.id;

      if (this._selectedRuleFlag === flag){
        button.checked = false;
        this._selectedRuleFlag = null;
      } else {
        this._selectedRuleFlag = flag;
      }
      this._fireActivatedRoundFlag(this._selectedRuleFlag);
      this.setAttribute("selectedRuleFlag", this._selectedRuleFlag);
    }

    get selectedRuleFlag (){
      return this._selectedRuleFlag;
    }
    
    set selectedRuleFlag (flag){

    }

    _fireActivatedRoundFlag(flag) {
      this.dispatchEvent(new CustomEvent('activatedRoundFlag', {detail:{ flag: flag }, bubbles: true}))
    }
    
    // Called when element is inserted in DOM
    connectedCallback() {
      window.addEventListener('roundChanged' , e => { 
        var radioButton = this._root.querySelector(":checked");
        if (radioButton){
          radioButton.checked=false;
        }
        this._selectedRuleFlag = null;
        this._fireActivatedRoundFlag(null);
      } , false);
    };

  }
 
  customElements.define('wcmpocha-rules', wcmPochaRules);
})();